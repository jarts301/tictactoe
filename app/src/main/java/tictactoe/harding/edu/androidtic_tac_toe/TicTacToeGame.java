package tictactoe.harding.edu.androidtic_tac_toe;

import java.util.InputMismatchException;
import java.util.Random;

/**
 * Created by Jarts on 28/08/2016.
 */
public class TicTacToeGame {

    public char mBoard[] = new char[9];
    public static final int BOARD_SIZE = 9;

    public static final int PLAYER_ONE_IDENTIFIER = 0;
    public static final int PLAYER_TWO_IDENTIFIER = 1;

    public static final int TIE = 1;
    public static final int ON_GAME = 0;
    public static final int WINNER_ONE = 2;
    public static final int WINNER_TWO = 3;

    public static final char PLAYER_ONE = 'X';
    public static final char PLAYER_TWO = 'O';
    public static final char OPEN_SPOT = ' ';

    private Random mRand;

    public char turn;

    public enum DifficultyLevel {Easy, Harder, Expert};

    private DifficultyLevel mDifficultyLevel = DifficultyLevel.Expert;

    public TicTacToeGame(int startPlayer) {

        mRand = new Random();
        clearBoard();

        turn = startPlayer == PLAYER_ONE_IDENTIFIER ? PLAYER_ONE : PLAYER_TWO;
        int  win = ON_GAME;

        /*while (win == ON_GAME)
        {

            if (turn == PLAYER_ONE)
            {
                //TODO: USER MOVE
            }
            else
                getComputerMove();

            turn = turn == PLAYER_ONE ? PLAYER_TWO : PLAYER_ONE;
            win = checkForWinner();
        }*/

    }

    public char[] getmBoard() {
        return mBoard;
    }

    public int checkForWinner() {

        for (int i = 0; i <= 6; i += 3)	{
            if (mBoard[i] == PLAYER_ONE && mBoard[i+1] == PLAYER_ONE && mBoard[i+2]== PLAYER_ONE)
                return WINNER_ONE;
            if (mBoard[i] == PLAYER_TWO && mBoard[i+1]== PLAYER_TWO && mBoard[i+2] == PLAYER_TWO)
                return WINNER_TWO;
        }

        for (int i = 0; i <= 2; i++) {
            if (mBoard[i] == PLAYER_ONE && mBoard[i+3] == PLAYER_ONE && mBoard[i+6]== PLAYER_ONE)
                return WINNER_ONE;
            if (mBoard[i] == PLAYER_TWO && mBoard[i+3] == PLAYER_TWO && mBoard[i+6]== PLAYER_TWO)
                return WINNER_TWO;
        }

        if ((mBoard[0] == PLAYER_ONE && mBoard[4] == PLAYER_ONE && mBoard[8] == PLAYER_ONE) ||
                (mBoard[2] == PLAYER_ONE && mBoard[4] == PLAYER_ONE && mBoard[6] == PLAYER_ONE))
            return WINNER_ONE;
        if ((mBoard[0] == PLAYER_TWO && mBoard[4] == PLAYER_TWO && mBoard[8] == PLAYER_TWO) ||
                (mBoard[2] == PLAYER_TWO && mBoard[4] == PLAYER_TWO && mBoard[6] == PLAYER_TWO))
            return WINNER_TWO;

        for (int i = 0; i < BOARD_SIZE; i++) {
            if (mBoard[i] != PLAYER_ONE && mBoard[i] != PLAYER_TWO)
                return ON_GAME;
        }

        return TIE;
    }

    public void clearBoard(){
        for (int i = 0; i < BOARD_SIZE; i++) {
            mBoard[i] = OPEN_SPOT;
        }
    }

    public void setMove(char player, int location){
        mBoard[location] = player;
    }

    public int getComputerMove() {
        int move = -1;
        if (mDifficultyLevel == DifficultyLevel.Easy)
            move = getRandomMove();
        else if (mDifficultyLevel == DifficultyLevel.Harder) {
            move = getWinningMove();
            if (move == -1)
                move = getRandomMove();
        }
        else if (mDifficultyLevel == DifficultyLevel.Expert) {
            move = getWinningMove();
            if (move == -1)
                move = getBlockingMove();
            if (move == -1)
                move = getRandomMove();
        }
        setMove(PLAYER_TWO, move);
        return move;
    }

    private int getRandomMove(){
        int move;
        do {
            move = mRand.nextInt(BOARD_SIZE);
        } while (mBoard[move] == PLAYER_ONE || mBoard[move] == PLAYER_TWO);
        return move;
    }

    private int getBlockingMove(){
        int move = -1;
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (mBoard[i] != PLAYER_ONE && mBoard[i] != PLAYER_TWO) {
                char curr = mBoard[i];
                setMove(PLAYER_ONE, i);
                if (checkForWinner() == WINNER_ONE) {
                    return i;
                } else
                    setMove(curr, i);
            }
        }
        return move;
    }

    private int getWinningMove(){
        int move = -1;
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (mBoard[i] != PLAYER_ONE && mBoard[i] != PLAYER_TWO) {
                char curr = mBoard[i];
                setMove(PLAYER_TWO, i);
                if (checkForWinner() == WINNER_TWO)
                    return i;
                else
                    setMove(curr, i);
            }
        }
        return move;
    }

    public DifficultyLevel getDifficultyLevel() {
        return mDifficultyLevel;
    }

    public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
        mDifficultyLevel = difficultyLevel;
    }

}
