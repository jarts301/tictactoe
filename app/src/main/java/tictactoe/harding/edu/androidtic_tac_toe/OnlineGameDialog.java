package tictactoe.harding.edu.androidtic_tac_toe;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by raham on 28/10/2016.
 */

public class OnlineGameDialog extends DialogFragment {

    public static OnlineGame onlineGame;
    public String idDuelo;

    static OnlineGameDialog newInstance(OnlineGame og) {
        OnlineGameDialog f = new OnlineGameDialog();
        Bundle args = new Bundle();
        //args.putString("comando", comando);
        f.setArguments(args);
        onlineGame = og;
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());



        //Dialog res =  builder.create();

        /*int segundos=45;
        while (segundos<=0){
            try {
                Thread.sleep(1 * 1000);
                if(!buscando.getIdDuelo().equals("NONE")){
                    idDuelo = buscando.getIdDuelo();
                    break;
                }
                segundos--;
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        /*onlineGame = new OnlineGame();
        try {
            idDuelo = (String)onlineGame.execute(getArguments().getString("comando")).get();
            Log.v("DueloInicio: ",idDuelo);
        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }*/

        builder.setMessage(R.string.online_game_search)
                .setNegativeButton(R.string.online_game_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        OnlineGame onlineGameSalir = new OnlineGame();
                        onlineGameSalir.execute("SALIR,null");
                        onlineGame.onPostExecute(null);
                        onlineGame.cancel(true);
                        //Log.v("mens",onlineGame.getStatus().name());
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
