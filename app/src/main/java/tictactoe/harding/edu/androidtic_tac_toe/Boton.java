package tictactoe.harding.edu.androidtic_tac_toe;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Jarts on 1/10/2016.
 */
public class Boton {
    private boolean activo;
    private Rect rect;
    private Bitmap bitmap;
    private int bitmapId;

    public Boton(Rect rect){
        this.activo = false;
        this.rect = rect;
        this.bitmapId=0;
    }
    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }


    public int getBitmapId() {
        return bitmapId;
    }

    public void setBitmapId(int bitmapId) {
        this.bitmapId = bitmapId;
    }
}
