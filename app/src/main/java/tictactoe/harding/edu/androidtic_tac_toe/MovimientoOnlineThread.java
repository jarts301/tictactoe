package tictactoe.harding.edu.androidtic_tac_toe;

import android.util.Log;

/**
 * Created by raham on 30/10/2016.
 */

public class MovimientoOnlineThread extends Thread {

    private AndroidTicTacToeActivity actividad;
    private String comando;
    private int turno;

    @Override
    public void run() {
        try {
            if(turno==1) {//Enviar
                OnlineGame onlineGame = new OnlineGame();
                onlineGame.execute(comando);
            }else{//Esperar
                OnlineGame onlineGame = new OnlineGame();
                Integer move = Integer.parseInt(((String[]) onlineGame.execute(comando).get())[0]);

                actividad.myBoardButtons[move].setBitmap(actividad.mBoardView.getmComputerBitmap());
                actividad.myBoardButtons[move].setBitmapId(2);
                actividad.setMove(TicTacToeGame.PLAYER_TWO, move);
                actividad.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        actividad.myInfoTextView.setText(R.string.turn_one);
                    }
                });
                actividad.myBoardButtons[move].setActivo(true);
                if (actividad.mSoundOn) {
                    actividad.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            actividad.mComputerMediaPlayer.start();
                        }
                    });
                }
                actividad.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        actividad.mBoardView.invalidate();
                    }
                });
                actividad.oponentePlaying = false;

                if (actividad.myGame.checkForWinner() == actividad.myGame.WINNER_TWO) {
                    actividad.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            actividad.myInfoTextView.setText(R.string.winner_two_h);
                            actividad.mBoardView.invalidate();
                            if (actividad.mSoundOn) {
                                actividad.mp3Pierde.start();
                            }
                        }
                    });
                    actividad.inGame = false;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setComando(String comando) {
        this.comando = comando;
    }

    public void setActividad(AndroidTicTacToeActivity actividad) {
        this.actividad = actividad;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }
}
