package tictactoe.harding.edu.androidtic_tac_toe;

import android.os.Handler;
import android.util.Log;

/**
 * Created by raham on 30/10/2016.
 */

public class BuscarDueloThread extends Thread {

    private OnlineGame onlineGame;
    private String comando;
    private AndroidTicTacToeActivity actividad;
    private OnlineGameDialog dialog;

    public BuscarDueloThread(AndroidTicTacToeActivity actividad,OnlineGameDialog dialog,OnlineGame onlineGame, String comando){
        this.onlineGame = onlineGame;
        this.comando = comando;
        this.actividad = actividad;
        this.actividad.idDuelo = "NONE";
        this.dialog = dialog;
    }

    @Override
    public void run() {
            try {
                String[] resultado=(String[])onlineGame.execute(comando).get();
                this.actividad.idJugador = resultado[0];
                this.actividad.idDuelo = resultado[1];
                this.actividad.puertoOnline = resultado[2];
                this.actividad.ipOnline = resultado[3];
                this.actividad.enLinea=true;
                this.actividad.startNewGame();
                this.dialog.dismiss();
                Log.v("DueloInicio: ",this.actividad.idDuelo);
            }catch (Exception e){
                e.printStackTrace();
            }
    }

}
