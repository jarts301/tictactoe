package tictactoe.harding.edu.androidtic_tac_toe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Jarts on 27/09/2016.
 */
public class BoardView extends View {

    // Width of the board grid lines
    public static final int GRID_WIDTH = 6;
    private Bitmap mHumanBitmap;
    private Bitmap mComputerBitmap;
    private Paint mPaint;
    private TicTacToeGame mGame;
    private Boton[] botones;

    public BoardView(Context context) {
        super(context);
        initialize();
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public void initialize() {
        mHumanBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.x_img);
        mComputerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.o_img);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Determine the width and height of the View
        int boardWidth = getWidth();
        int boardHeight = getHeight();

        // Make thick, light gray lines
        mPaint.setColor(Color.LTGRAY);
        mPaint.setStrokeWidth(GRID_WIDTH);

        // Draw the two vertical board lines
        int cellWidth = boardWidth / 3;
        canvas.drawLine(cellWidth, 0, cellWidth, boardHeight, mPaint);
        canvas.drawLine(cellWidth * 2, 0, cellWidth * 2, boardHeight, mPaint);
        canvas.drawLine(0,cellWidth, boardHeight, cellWidth , mPaint);
        canvas.drawLine(0,cellWidth * 2, boardHeight, cellWidth * 2 , mPaint);

        // Draw all the X and O images
        for (int i = 0; i < botones.length; i++) {

            if(botones[i].getActivo()) {

                canvas.drawBitmap(botones[i].getBitmap(),
                        null,  // src
                        botones[i].getRect(),  // dest
                        null);
            }

            // Define the boundaries of a destination rectangle for the image
            /*int left = boardHeight/3;
            int top = boardHeight/3;
            int right = boardHeight/3;
            int bottom = boardHeight/3;

            if (mGame != null && mGame.turn == TicTacToeGame.PLAYER_ONE_IDENTIFIER) {
                canvas.drawBitmap(mHumanBitmap,
                        null,  // src
                        new Rect(left, top, right, bottom),  // dest
                        null);
            }
            else if (mGame != null && mGame.turn == TicTacToeGame.PLAYER_TWO_IDENTIFIER) {
                canvas.drawBitmap(mComputerBitmap,
                        null,  // src
                        new Rect(left, top, right, bottom),  // dest
                        null);
            }*/
        }

    }

    public void setGame(TicTacToeGame game) {
        mGame = game;
    }

    public int getBoardCellWidth() {
        return getWidth() / 3;
    }

    public int getBoardCellHeight() {
        return getHeight() / 3;
    }


    public Bitmap getmComputerBitmap() {
        return mComputerBitmap;
    }

    public void setmComputerBitmap(Bitmap mComputerBitmap) {
        this.mComputerBitmap = mComputerBitmap;
    }

    public Bitmap getmHumanBitmap() {
        return mHumanBitmap;
    }

    public void setmHumanBitmap(Bitmap mHumanBitmap) {
        this.mHumanBitmap = mHumanBitmap;
    }

    public Boton[] getBotones() {
        return botones;
    }

    public void setBotones(Boton[] botones) {
        this.botones = botones;
    }
}
