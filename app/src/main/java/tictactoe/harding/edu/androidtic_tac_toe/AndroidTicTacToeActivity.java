package tictactoe.harding.edu.androidtic_tac_toe;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Random;

public class AndroidTicTacToeActivity extends AppCompatActivity {

    //static final int DIALOG_DIFFICULTY_ID = 0;
    static final int DIALOG_QUIT_ID = 1;
    static final int DIALOG_ABOUT_ID = 2;

    public static final int COLOR_RED = Color.rgb(255, 0, 0);
    public static final int COLOR_GREEN = Color.rgb(0, 255, 0);

    public static TicTacToeGame myGame;

    private SharedPreferences mPrefs;
    public Boton[] myBoardButtons;
    private Button btnRestart, btnSettings, btnQuit;
    public TextView myInfoTextView;
    private TextView humanScore;
    private TextView tiesScore;
    private TextView androidScore;

    private Random turnRand;
    private int startPlayer;
    public String idDuelo;
    public String idJugador;
    public String puertoOnline;
    public String ipOnline;
    public boolean enLinea;

    public BoardView mBoardView;
    public boolean inGame=true;

    private int valTiesScore=0;
    private int valHumanScore=0;
    private int valAndroidScore=0;

    public boolean machinePlaying,oponentePlaying,mSoundOn;

    MediaPlayer mHumanMediaPlayer;
    MediaPlayer mComputerMediaPlayer;
    MediaPlayer mp3Empate, mp3Gana, mp3Pierde;

    private int move,moveFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idDuelo="NONE";
        idJugador="NONE";
        enLinea = false;
        setContentView(R.layout.main);
        //mPrefs = getSharedPreferences("ttt_prefs", MODE_PRIVATE);
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        HashMap<String, String> userData = new HashMap<>();

        userData.put("player_1", "" + TicTacToeGame.PLAYER_ONE_IDENTIFIER);
        userData.put("deviceId_1", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

        humanScore = (TextView) findViewById(R.id.humanScore);
        tiesScore = (TextView) findViewById(R.id.tiesScore);
        androidScore = (TextView) findViewById(R.id.androidScore);

        tiesScore.setText("0");
        humanScore.setText("0");
        androidScore.setText("0");

        machinePlaying = false;
        oponentePlaying = false;
        mSoundOn = true;

        myBoardButtons = new Boton[TicTacToeGame.BOARD_SIZE];

        myBoardButtons[0] = new Boton(new Rect(0,0,150,150));
        myBoardButtons[1] = new Boton(new Rect(151,0,299,150));
        myBoardButtons[2] = new Boton(new Rect(300,0,450,150));
        myBoardButtons[3] = new Boton(new Rect(0,150,150,300));
        myBoardButtons[4] = new Boton(new Rect(151,150,299,300));
        myBoardButtons[5] = new Boton(new Rect(300,150,450,300));
        myBoardButtons[6] = new Boton(new Rect(0,300,150,450));
        myBoardButtons[7] = new Boton(new Rect(151,300,299,450));
        myBoardButtons[8] = new Boton(new Rect(300,300,450,450));

        btnRestart = (Button) findViewById(R.id.btnRestartMain);
        btnSettings = (Button) findViewById(R.id.btnSettings);
        btnQuit = (Button) findViewById(R.id.btnExit);

        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewGame();
            }
        });

        btnSettings.setOnClickListener(new OpcionesBotonClickListener(this));

        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog(DIALOG_QUIT_ID);
            }
        });

        myInfoTextView = (TextView) findViewById(R.id.information);

        turnRand = new Random();
        startPlayer = turnRand.nextInt(2);

        myGame = new TicTacToeGame(startPlayer);

        mBoardView = (BoardView) findViewById(R.id.board);
        mBoardView.setGame(myGame);
        mBoardView.setBotones(myBoardButtons);

        Handler handler = new Handler();
        mBoardView.setOnTouchListener(new ListenerOntouch(this,handler));

        //mPrefs = getSharedPreferences("ttt_prefs", MODE_PRIVATE);

        // Restore the scores
        valHumanScore = mPrefs.getInt("valHumanScore", 0);
        valAndroidScore = mPrefs.getInt("valAndroidScore", 0);
        valTiesScore = mPrefs.getInt("valTiesScore", 0);
        mSoundOn = mPrefs.getBoolean("sound", true);
        tiesScore.setText(String.valueOf(valTiesScore));
        androidScore.setText(String.valueOf(valAndroidScore));
        humanScore.setText(String.valueOf(valHumanScore));

        String difficultyLevel = mPrefs.getString("difficulty",
                getResources().getString(R.string.difficulty_harder));
        if (difficultyLevel.equals(getResources().getString(R.string.difficulty_easy)))
            myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
        else if (difficultyLevel.equals(getResources().getString(R.string.difficulty_harder)))
            myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
        else
            myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);

        if (savedInstanceState == null) {
            startNewGame();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(machinePlaying){
            myBoardButtons[moveFinal].setActivo(true);
            machinePlaying = false;
        }

        reservarDatosBotones(outState);
        outState.putBoolean("inGame", inGame);
        outState.putString("myInfoTextView", myInfoTextView.getText().toString());
        outState.putString("humanScore", humanScore.getText().toString());
        outState.putString("tiesScore", tiesScore.getText().toString());
        outState.putString("androidScore", androidScore.getText().toString());
        outState.putInt("valHumanScore", valHumanScore);
        outState.putInt("valTiesScore", valTiesScore);
        outState.putInt("valAndroidScore", valAndroidScore);
        //outState.putBoolean("mSoundOn", mSoundOn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cargarDatosBotones(savedInstanceState);
        inGame = savedInstanceState.getBoolean("inGame");
        myInfoTextView.setText(savedInstanceState.getString("myInfoTextView"));
        humanScore.setText(savedInstanceState.getString("humanScore"));
        tiesScore.setText(savedInstanceState.getString("tiesScore"));
        androidScore.setText(savedInstanceState.getString("androidScore"));
        valHumanScore = savedInstanceState.getInt("valHumanScore");
        valTiesScore = savedInstanceState.getInt("valTiesScore");
        valAndroidScore = savedInstanceState.getInt("valAndroidScore");
        //mSoundOn = savedInstanceState.getBoolean("mSoundOn");
    }

    public void reservarDatosBotones(Bundle outState){
        String botonesActivo = "";
        String botonesBitmap = "";
        for (int i=0;i<myBoardButtons.length;i++){
           botonesActivo= botonesActivo + myBoardButtons[i].getActivo() + (i<myBoardButtons.length-1?",":"");
            botonesBitmap= botonesBitmap + (myBoardButtons[i].getBitmapId()==1?1:myBoardButtons[i].getBitmapId()==2?2:0) + (i<myBoardButtons.length-1?",":"");
        }
        outState.putString("botonesActivo",botonesActivo);
        outState.putString("botonesBitmap",botonesBitmap);
        //Toast.makeText(getApplicationContext(), botonesActivo.toString(), Toast.LENGTH_LONG).show();
    }

    public void cargarDatosBotones(Bundle savedInstanceState){
        String[] botonesActivo = savedInstanceState.getString("botonesActivo").split(",");
        String[] botonesBitmap = savedInstanceState.getString("botonesBitmap").split(",");
        for (int i=0;i<botonesActivo.length;i++){
            myBoardButtons[i].setActivo(botonesActivo[i].equals("true") ? true : false);
            myBoardButtons[i].setBitmap(botonesBitmap[i].equals("1") ? mBoardView.getmHumanBitmap() : botonesBitmap[i].equals("2") ? mBoardView.getmComputerBitmap() : null);
            myBoardButtons[i].setBitmapId(botonesBitmap[i].equals("1") ? 1 : botonesBitmap[i].equals("2") ? 2 : 0);
            if(botonesBitmap[i].equals("1") || botonesBitmap[i].equals("2")) {
                setMove(botonesBitmap[i].equals("1") ? TicTacToeGame.PLAYER_ONE : botonesBitmap[i].equals("2") ? TicTacToeGame.PLAYER_TWO : null , i);
            }
        }
        mBoardView.invalidate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHumanMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.human);
        mComputerMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.machine);
        mp3Empate = MediaPlayer.create(getApplicationContext(), R.raw.empate);
        mp3Gana = MediaPlayer.create(getApplicationContext(), R.raw.gana);
        mp3Pierde = MediaPlayer.create(getApplicationContext(), R.raw.pierde);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHumanMediaPlayer.release();
        mComputerMediaPlayer.release();
        mp3Empate.release();
        mp3Gana.release();
        mp3Pierde.release();

        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("valHumanScore", valHumanScore);
        ed.putInt("valTiesScore", valTiesScore);
        ed.putInt("valAndroidScore", valAndroidScore);
        ed.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Save the current scores
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("valHumanScore", valHumanScore);
        ed.putInt("valTiesScore", valTiesScore);
        ed.putInt("valAndroidScore", valAndroidScore);
        ed.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_game:
                startNewGame();
                return true;
            case R.id.online_game:
                OnlineGame onlineGame = new OnlineGame();
                OnlineGameDialog dialog = (new OnlineGameDialog()).newInstance(onlineGame);
                dialog.show(getFragmentManager(),"dialog");
                dialog.setCancelable(false);
                BuscarDueloThread buscando = new BuscarDueloThread(this,dialog,onlineGame,"COLA,null");
                buscando.start();
                return true;
            case R.id.settings:
                startActivityForResult(new Intent(this, Opciones.class), 0);
                return true;
            case R.id.quit:
                showAlertDialog(DIALOG_QUIT_ID);
                return true;
            case R.id.about:
                showAlertDialog(DIALOG_ABOUT_ID);
                return true;
        }
        return false;
    }

    private void showAlertDialog( int id ) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            /*case DIALOG_DIFFICULTY_ID:
                builder.setTitle(R.string.difficulty_choose);
                final CharSequence[] levels = {
                        getResources().getString(R.string.difficulty_easy),
                        getResources().getString(R.string.difficulty_harder),
                        getResources().getString(R.string.difficulty_expert)};
                builder.setSingleChoiceItems(levels, difficulty,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                dialog.dismiss();
                                difficulty = item;
                                Toast.makeText(getApplicationContext(), levels[item], Toast.LENGTH_SHORT).show();
                                startNewGame();
                            }
                        });
                dialog = builder.create();
                break;*/
            case DIALOG_QUIT_ID:
                builder.setMessage(R.string.quit_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;
            case DIALOG_ABOUT_ID:
                builder.setView(R.layout.about);
                builder.setTitle(R.string.about);
                builder.setPositiveButton("OK", null);
                dialog = builder.create();
                break;
        }
        dialog.show();
    }

    public void startNewGame() {
        inGame=true;
        for (int i=0;i<myBoardButtons.length;i++){
            myBoardButtons[i].setActivo(false);
            myBoardButtons[i].setBitmapId(0);
            myBoardButtons[i].setBitmap(null);
        }

        if(!enLinea) {
            mBoardView.invalidate();   // Redraw the board
            turnRand = new Random();
            startPlayer = turnRand.nextInt(2);
            myGame = new TicTacToeGame(startPlayer);
            String difficultyLevel = mPrefs.getString("difficulty",
                    getResources().getString(R.string.difficulty_harder));

            if (difficultyLevel.equals(getResources().getString(R.string.difficulty_easy)))
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
            else if (difficultyLevel.equals(getResources().getString(R.string.difficulty_harder)))
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
            else
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);

            myInfoTextView.setText(R.string.start_one);
            if( startPlayer == TicTacToeGame.PLAYER_ONE_IDENTIFIER )
                myInfoTextView.setText(R.string.start_one);
            else{
                myInfoTextView.setText(R.string.turn_two);
                int move = myGame.getComputerMove();
                myBoardButtons[move].setActivo(true);
                myBoardButtons[move].setBitmap(mBoardView.getmComputerBitmap());
                myBoardButtons[move].setBitmapId(2);
                setMove(TicTacToeGame.PLAYER_TWO, move);
                myInfoTextView.setText(R.string.turn_one);
            }

        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBoardView.invalidate();
                }
            });
            startPlayer = Integer.valueOf(idDuelo.substring(0, 1));
            if (startPlayer == TicTacToeGame.PLAYER_ONE_IDENTIFIER){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        myInfoTextView.setText(R.string.start_one);
                    }
                });
                oponentePlaying =false;
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        myInfoTextView.setText(R.string.turn_two_h);
                    }
                });
                oponentePlaying =true;
                MovimientoOnlineThread mot2 = new MovimientoOnlineThread();
                mot2.setActividad(this);
                mot2.setTurno(2);
                mot2.setComando("ESPERA,"+ipOnline+","+puertoOnline);
                mot2.start();
            }
        }

    }

    public void setMove(char player, int location) {
        myGame.setMove(player, location);
        /*myBoardButtons[location].setEnabled(false);
        myBoardButtons[location].setText(String.valueOf(player));

        myBoardButtons[location].setTextColor(player == TicTacToeGame.PLAYER_ONE ? COLOR_GREEN : COLOR_RED);*/
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("valHumanScore", valHumanScore);
        ed.putInt("valTiesScore", valTiesScore);
        ed.putInt("valAndroidScore", valAndroidScore);
        ed.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RESULT_CANCELED) {
            // Apply potentially new settings

            mSoundOn = mPrefs.getBoolean("sound", true);

            String difficultyLevel = mPrefs.getString("difficulty",
                    getResources().getString(R.string.difficulty_harder));

            if (difficultyLevel.equals(getResources().getString(R.string.difficulty_easy)))
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
            else if (difficultyLevel.equals(getResources().getString(R.string.difficulty_harder)))
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
            else
                myGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);
        }
    }

    public class OpcionesBotonClickListener implements View.OnClickListener
    {

        AndroidTicTacToeActivity actividad;
        public OpcionesBotonClickListener(AndroidTicTacToeActivity actividad) {
            this.actividad = actividad;
        }

        @Override
        public void onClick(View v)
        {
            startActivityForResult(new Intent(actividad, Opciones.class), 0);
        }

    }

    public class ListenerOntouch implements View.OnTouchListener{

        AndroidTicTacToeActivity actividad;
        Handler handler;

        public ListenerOntouch(AndroidTicTacToeActivity actividad, Handler handler){
            this.actividad = actividad;
            this.handler = handler;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            for (int i = 0; i < myBoardButtons.length; i++) {
                if (myBoardButtons[i].getRect().contains((int) event.getX(), (int) event.getY())) {
                    if (inGame) {
                        if (!enLinea) {
                            if (!myBoardButtons[i].getActivo() && !machinePlaying) {
                                myBoardButtons[i].setActivo(true);
                                myBoardButtons[i].setBitmap(mBoardView.getmHumanBitmap());
                                myBoardButtons[i].setBitmapId(1);
                                setMove(TicTacToeGame.PLAYER_ONE, i);
                                if (mSoundOn) {
                                    mHumanMediaPlayer.start();
                                }
                                mBoardView.invalidate();

                                if (myGame.checkForWinner() != myGame.WINNER_ONE && myGame.checkForWinner() != myGame.TIE) {
                                    machinePlaying = true;
                                    myInfoTextView.setText(R.string.turn_two);
                                    move = myGame.getComputerMove();
                                    myBoardButtons[move].setBitmap(mBoardView.getmComputerBitmap());
                                    myBoardButtons[move].setBitmapId(2);
                                    setMove(TicTacToeGame.PLAYER_TWO, move);
                                    myInfoTextView.setText(R.string.turn_one);
                                    moveFinal = move;

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            myBoardButtons[move].setActivo(true);
                                            try {
                                                if (mSoundOn) {
                                                    mComputerMediaPlayer.start();
                                                }
                                            } catch (Exception e) {
                                            }
                                            mBoardView.invalidate();
                                            machinePlaying = false;
                                        }
                                    }, 500);

                                    if (myGame.checkForWinner() == myGame.WINNER_TWO) {
                                        valAndroidScore++;
                                        myInfoTextView.setText(R.string.winner_two);
                                        inGame = false;
                                        mBoardView.invalidate();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (mSoundOn) {
                                                        mp3Pierde.start();
                                                    }
                                                } catch (Exception e) {
                                                }
                                            }
                                        }, 400);
                                    }
                                }
                                if (myGame.checkForWinner() == myGame.WINNER_ONE) {
                                    valHumanScore++;
                                    String defaultMessage = getResources().getString(R.string.winner_one);
                                    myInfoTextView.setText(mPrefs.getString("victory_message", defaultMessage));

                                    inGame = false;
                                    mBoardView.invalidate();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (mSoundOn) {
                                                    mp3Gana.start();
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 400);
                                }
                                if (myGame.checkForWinner() == myGame.TIE) {
                                    valTiesScore++;
                                    myInfoTextView.setText(R.string.game_tied);
                                    inGame = false;
                                    mBoardView.invalidate();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (mSoundOn) {
                                                    mp3Empate.start();
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 400);
                                }
                                break;
                            }
                        }else { // Si esta jugando en linea
                            if (!myBoardButtons[i].getActivo() && !oponentePlaying) {
                                myBoardButtons[i].setActivo(true);
                                myBoardButtons[i].setBitmap(mBoardView.getmHumanBitmap());
                                myBoardButtons[i].setBitmapId(1);
                                setMove(TicTacToeGame.PLAYER_ONE, i);
                                if (mSoundOn) {
                                    mHumanMediaPlayer.start();
                                }
                                mBoardView.invalidate();
                                MovimientoOnlineThread mot = new MovimientoOnlineThread();
                                mot.setActividad(actividad);
                                mot.setTurno(1);
                                mot.setComando("MOV,"+idDuelo.substring(1,33)+","+idJugador+","+i);
                                mot.start();

                                if (myGame.checkForWinner() != myGame.WINNER_ONE && myGame.checkForWinner() != myGame.TIE) {
                                    oponentePlaying = true;
                                    myInfoTextView.setText(R.string.turn_two_h);
                                    MovimientoOnlineThread mot2 = new MovimientoOnlineThread();
                                    mot2.setActividad(actividad);
                                    mot2.setTurno(2);
                                    mot2.setComando("ESPERA,"+ipOnline+","+puertoOnline);
                                    mot2.start();
                                    //Tal vez actualizando datos en el servidor si funcione

                                    //move = myGame.getComputerMove();
                                    /*myBoardButtons[move].setBitmap(mBoardView.getmComputerBitmap());
                                    myBoardButtons[move].setBitmapId(2);
                                    setMove(TicTacToeGame.PLAYER_TWO, move);
                                    myInfoTextView.setText(R.string.turn_one);
                                    moveFinal = move;
                                    myBoardButtons[move].setActivo(true);
                                    if (mSoundOn) {
                                        mComputerMediaPlayer.start();
                                    }
                                    mBoardView.invalidate();
                                    oponentePlaying = false;

                                    if (myGame.checkForWinner() == myGame.WINNER_TWO) {
                                        valAndroidScore++;
                                        myInfoTextView.setText(R.string.winner_two);
                                        inGame = false;
                                        mBoardView.invalidate();
                                        if (mSoundOn) {
                                            mp3Pierde.start();
                                        }
                                    }*/

                                }
                                if (myGame.checkForWinner() == myGame.WINNER_ONE) {
                                    valHumanScore++;
                                    String defaultMessage = getResources().getString(R.string.winner_one);
                                    myInfoTextView.setText(mPrefs.getString("victory_message", defaultMessage));

                                    inGame = false;
                                    mBoardView.invalidate();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (mSoundOn) {
                                                    mp3Gana.start();
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 400);
                                }
                                if (myGame.checkForWinner() == myGame.TIE) {
                                    valTiesScore++;
                                    myInfoTextView.setText(R.string.game_tied);
                                    inGame = false;
                                    mBoardView.invalidate();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (mSoundOn) {
                                                    mp3Empate.start();
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 400);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            tiesScore.setText(String.valueOf(valTiesScore));
            androidScore.setText(String.valueOf(valAndroidScore));
            humanScore.setText(String.valueOf(valHumanScore));
            return true;
        }
    }

}
