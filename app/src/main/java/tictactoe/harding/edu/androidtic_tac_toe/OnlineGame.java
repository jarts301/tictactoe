package tictactoe.harding.edu.androidtic_tac_toe;

import android.os.AsyncTask;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * Created by raham on 28/10/2016.
 */

public class OnlineGame extends AsyncTask{
    private final String serverIp = /*"192.168.0.4";*/ "52.204.1.104";
    private final int port = 9876;
    private DatagramSocket clientSocket;

    @Override
    protected String[] doInBackground(Object... params) {
        String[] res=new String[]{"","","",""};
        try {
            if(((String)params[0]).split(",")[0].equals("COLA")) {
                DatagramSocket clientSocket = new DatagramSocket();
                InetAddress IPAddress = InetAddress.getByName(serverIp);
                byte[] sendData = new byte[1024];
                byte[] receiveData = new byte[1024];
                sendData = ((String) params[0]).getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                clientSocket.send(sendPacket);
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                String modifiedSentence = new String(receivePacket.getData());
                res[0] = modifiedSentence.substring(2, 34);
                Log.v("Mensaje", "Par o impar" + modifiedSentence.charAt(0));

                if (String.valueOf(modifiedSentence.charAt(0)).equals("1")) {
                    sendData = new byte[1024];
                    receiveData = new byte[1024];
                    sendData = ("NONE").getBytes();
                    sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    clientSocket.send(sendPacket);
                    receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    clientSocket.receive(receivePacket);
                    modifiedSentence = new String(receivePacket.getData());
                    res[1] = modifiedSentence.substring(0, 33);
                    res[2] = modifiedSentence.substring(34).split(",")[0];
                    res[3] = modifiedSentence.substring(34).split(",")[1];
                    Log.v("Mensaje", "DueloId" + res[1]+","+res[2]+","+res[3]);
                } else {
                    IPAddress = InetAddress.getByName(serverIp);
                    sendData = new byte[1024];
                    receiveData = new byte[1024];
                    sendData = ("INICIO,null").getBytes();
                    sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    clientSocket.send(sendPacket);
                    receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    clientSocket.receive(receivePacket);
                    modifiedSentence = new String(receivePacket.getData());
                    res[1] = modifiedSentence.substring(0, 33);
                    res[2] = modifiedSentence.substring(34).split(",")[0];
                    res[3] = modifiedSentence.substring(34).split(",")[1];
                    Log.v("Mensaje", "DueloId" + res[1]+","+res[2]+","+res[3]);
                }
                clientSocket.close();
            }
            if(((String)params[0]).split(",")[0].equals("SALIR") || ((String)params[0]).split(",")[0].equals("MOV") ) {
                DatagramSocket clientSocket = new DatagramSocket();
                InetAddress IPAddress = InetAddress.getByName(serverIp);
                byte[] sendData = new byte[1024];
                sendData = ((String) params[0]).getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                clientSocket.send(sendPacket);
                clientSocket.close(); //Acá mirar porque el jugador nunca recibe el mensaje de retorno
            }
            if(((String)params[0]).split(",")[0].equals("ESPERA")) {
                DatagramSocket clientSocket = new DatagramSocket(Integer.valueOf(((String)params[0]).split(",")[2]));
                //InetAddress IPAddress = InetAddress.getByName(serverIp);
                //byte[] sendData = new byte[1024];
                byte[] receiveData = new byte[1024];
                //sendData = ("NONE").getBytes();
                //DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                //clientSocket.send(sendPacket);
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                //InetSocketAddress address = new InetSocketAddress(((String)params[0]).split(",")[1], Integer.valueOf (((String)params[0]).split(",")[2]));
                //clientSocket.bind(address);
                //Log.v("Datos clientSocket",clientSocket2.getInetAddress()+":"+clientSocket2.getPort());
                clientSocket.receive(receivePacket);
                String modifiedSentence = new String(receivePacket.getData());
                res[0] = modifiedSentence.substring(0, 1);
                Log.v("Mensaje", "Index mov" + modifiedSentence);
                clientSocket.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onPostExecute(Object o) {
        //clientSocket.close();
    }
}
